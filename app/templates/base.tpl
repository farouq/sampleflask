{% extends 'bootstrap/base.html' %}
{% block styles %}
    {{super()}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
{% endblock %}

{% block scripts %}
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    {{super()}}
{% endblock %}

{% block title %}
    {% if title %}{{ title }} - Web app{% else %}WebApp{% endif %}
{% endblock %}

{% block navbar %}
<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-3">
    <a class="navbar-brand" href="#"><i class="fas fa-cogs"></i> CallCenter Tools</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            {% if current_user.is_authenticated %}
                <li class="nav-item"><a class='nav-link' href="{{ url_for('main.index') }}"><i class="fas fa-home"></i> Home</a></li>
                <li class="nav-item"><a class='nav-link' href="{{ url_for('leads.home') }}"><i class="fas fa-address-book"></i> Leads</a></li>
                <li class="nav-item"><a class='nav-link' href="{{ url_for('auth.logout') }}"><i class="fas fa-unlock"></i> Logout</a></li>
            {% else %}
                <li class="nav-item"><a class='nav-link' href="{{ url_for('auth.login') }}"><i class="fas fa-user"></i> Login</a></li>
            {% endif %}
        </ul>
    </div>
</nav>
{% endblock %}

{% block content %}
    <div class="container-fluid">
        {% with messages = get_flashed_messages() %}
        {% if messages %}
            {% for message in messages %}
                <div class="alert alert-info" role="alert">{{ message }}</div>
            {% endfor %}
        {% endif %}
        {% endwith %}

        {# application content needs to be provided in the app_content block #}
        {% block app_content %}{% endblock %}
    </div>
{% endblock %}
