import os
basedir = os.path.abspath(os.path.dirname(__file__))

# Declaring configuration variables.
class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'secret' 
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'app.db')
    ## Disable flask alchemy event system
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    #UPLOAD_FOLDER = os.path.join(basedir, '/app/_uploads')