from flask import render_template
from app import db
from app.errors import bp

## Handler to return 404 HTTP status along with according template
@bp.app_errorhandler(404)
def not_found_error(error):
    return render_template('errors/404.tpl'), 404

## Handler to return 500 HTTP status along with according template
@bp.app_errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('errors/500.tpl'), 500
