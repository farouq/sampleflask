from app import create_app, db
from app.auth.model import Users

app = create_app()

# Registering the shell context processor function.
@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'Users': Users}