{% extends "base.tpl" %}
{% import 'bootstrap/wtf.html' as wtf %}

{% block app_content %}
    <div class="row container-fluid mb-3">
        <h3><i class="fas fa-address-book"></i> Liste des opérations</h3>
    </div>

    {% if data %}
    <div class='container-fluid mb-3 alert alert-success'><i class="fas fa-info-circle"></i> <b>{{ data|length }}</b> Opération(s) trouvée(s) pour cette recherche</div>
    <div class='container-fluid'>
        <table class="table table-bordered table-hover table-sm w-100">
            <tr>
                <th>Id</th>
                <th>Utilisateur</th>
                <th>Nb° initial</th>
                <th>Nb° post-process</th>
                <th>Date</th>
                <th>Download</th>
            </tr>
        {% for item in data %}
            <tr>
                <td>{{ item.id }}</td>
                <td>{{ item.username }}</a></td>
                <td>{{ item.total_count }}</td>
                <td>{{ item.result_count }}</td>
                <td>{{ item.operation_date }}</td>
                <td><a href='{{ url_for('leads.download', filename=item.processed_link ) }}'><i class="fas fa-download"></i></a></td>
            </tr>
        {% endfor %}
        </table>
    </div>
    {% else %}
        <div class='container-fluid mb-3 alert alert-warning'><i class="fas fa-caret-circle"></i> Aucun résultat n'a été trouvé</div>
    {% endif %}

    <div style="position:absolute;bottom:5px;right:5px;margin:0;"><a class="btn btn-primary" href="{{ url_for('leads.form') }}"><i class="fas fa-plus-circle"></i></a></div>

    <div class="modal fade" id="modal_cross" tabindex="-1" role="dialog" aria-labelledby="modal_cross" aria-hidden="true">
    	<div class="modal-dialog modal-lg" role="document">
    		<div class="modal-content">
    			<div class="modal-header">
    				<h4 class="modal-title" id="exampleModalLabel">--</h4>
    				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    			</div>

    			<div class="modal-body">
    			</div>

    			<div class="modal-footer"></div>
    		</div>
    	</div>
    </div>
    <script>
    window.form = function ()
    	{
    	$('#modal_cross').find('.modal-title').html("<i class='fas fa-plus-circle'></i>");
    	$('#modal_cross').modal('show').find('.modal-body').load('');
    	}
    </script>
{% endblock %}
