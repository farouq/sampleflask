from app import db,login
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
import datetime

## Users class, defining user model and basic methods: repr, hash and check password
class Users(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    firstname = db.Column(db.String(32))
    lastname = db.Column(db.String(32))
    last_login = db.Column(db.DateTime)
    creation_date = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)

    def __repr__(self):
        return '<Id: {}, User : {}, Nom : {} {} >'.format( self.id, self.username, self.firstname, self.lastname )

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

## Fetching user's details after login.
@login.user_loader
def load_user(id):
    return Users.query.get(int(id))
