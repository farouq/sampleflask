{% extends "base.tpl" %}
{% import 'bootstrap/wtf.html' as wtf %}

{% block app_content %}
    <div class="row container w-25 mx-auto">
        <div class='container-fluid'>
            <div class="card">
                <h5 class="card-header info-color white-text text-center py-4">
                    <strong>Administration</strong>
                </h5>
                <div class="card-body py-2 px-2 text-center">
                    <form class="text-center form pt-3" method="post" role="form" action="">
                        {{ form.hidden_tag() }}
                        <input type="text" id="defaultLoginFormEmail" class="form-control mb-4" placeholder="Username" name="username">
                        <input type="password" id="defaultLoginFormPassword" class="form-control mb-4" placeholder="Password" name="password">
                        <div class="d-flex justify-content-around">
                            <div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="defaultLoginFormRemember">
                                    <label class="custom-control-label" for="defaultLoginFormRemember">Remember me</label>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-info btn-block my-4" type="submit">Sign in</button>
                    </form>
                </div>

                {% if error %}
                    <div class='container-fluid mt-2'>
                        <div class='alert alert-danger'><i class="fas fa-times-circle"></i> {{ error }}</div>
                    </div>
                {% endif %}
            </div>
        </div>
    </div>
{% endblock %}
