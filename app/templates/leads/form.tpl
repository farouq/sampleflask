{% extends "base.tpl" %}
{% import 'bootstrap/wtf.html' as wtf %}

{% block app_content %}
    <div class="row container-fluid mb-3">
        <div class='container-fluid'>
            <h3><i class="fas fa-users"></i> Nettoyer un fichier</h3>
        </div>
        {% if success %}
            <div class='container-fluid mt-2'>
                <div class='alert alert-success'>
                    {% if success > 0 %}
                        <i class="fas fa-check-circle"></i> fichiers croisés avec succés, {{ success }} entrée(s) supprimée(s), <a href='{{ url_for('leads.download', filename=file ) }}'>lien de telechargement</a>
                    {% else %}
                        <i class="fas fa-check-circle"></i> fichiers croisés avec succés, aucune entrée n'a été supprimée. </a>
                    {% endif %}
                </div>
            </div>
        {% else %}
            {{ wtf.quick_form(form) }}

            {% if error %}
                <div class='container-fluid mt-2'>
                    <div class='alert alert-danger'><i class="fas fa-times-circle"></i> {{ error }}</div>
                </div>
            {% endif %}
        {% endif %}
    </div>
{% endblock %}
